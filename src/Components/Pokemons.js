//Pokemons.js
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';

const Pokemons = props => {
  const [pokemons, setPokemons] = useState([]);
  const [searchfeild, setSearchfeild] = useState('');

  useEffect(() => {
    fetchPokemons();
  }, []);

  const fetchPokemons = () => {
    fetch('https://pokeapi.co/api/v2/pokemon?limit=500')
      .then(response => response.json())
      .then(
        pokemons => setPokemons(pokemons.results)
      )
      .then(
        console.log("pokemons.results: "+pokemons.results)
      );      
  };

  return (
    <View style={{backgroundColor: '#fff'}}>
      <View style={styles.searchCont}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.searchfeild}
          placeholder="Search Pokemons"
          onChangeText={value => setSearchfeild(value)}
          value={searchfeild}
        />
      </View>
      <ScrollView> 
        <View style={styles.container}>
        {pokemons
          .filter(pokemon =>
            pokemon.name.toLowerCase().includes(searchfeild.toLowerCase())
          )
          .map((pokemon, index) => {
            return (
              <TouchableOpacity
                activeOpacity={0.5}
                key={index}
                style={styles.card}
                onPress={() =>
                  props.navigation.navigate('Details', {
                    pokemon: pokemon.name,
                  })
                }>
                <Image
                  style={{width: 150, height: 150}}
                  source={{
                    uri: `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${
                      pokemon.name
                    }.png`,
                  }}
                />
                <View style={styles.type}>
                  <Text style={{ color: 'black', textAlign: 'center', fontWeight: '700', fontSize: 18 }}>{pokemon.name.toUpperCase()}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Pokemons;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginTop: 30,
  },
  card: {
    display: 'flex',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  searchCont: {
    backgroundColor: '#fff',
    position: 'absolute',
    width: '95%',
    zIndex: 1,
  },
  searchfeild: {
    marginLeft: 10,
    marginRight: 120,
    marginTop: 10,
    height: 40,
    borderWidth: 1,
    borderColor: '#000',
    backgroundColor: '#fff',
    textAlign: 'center',
    width: '100%',
    borderRadius: 5,
  },

  searchBar: {
    marginTop: 64,
    padding: 3,
    paddingLeft: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchBarInput: {
    fontSize: 15,
    flex: 1,
    height: 30,
  },
});