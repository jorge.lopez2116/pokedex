//Details.js
import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, ActivityIndicator} from 'react-native';
import { PokemonTypeImage } from '../Const/image'
// import ProgressBar from 'react-native-progress/Bar';

const Details = props => {
  const [details, setDetails] = useState([]);

  useEffect(() => {
    fetchPokemonDetails();
  }, []);

  const fetchPokemonDetails = () => {
    const {state} = props.navigation;
    fetch(`https://pokeapi.co/api/v2/pokemon/${state.params.pokemon}`)
      .then(res => res.json())
      .then(details => setDetails(details));
  };

  return details.name ? (
    <View style={{flex: 1, alignItems: 'center'}}>
      <Image
        style={styles.avatar}
        source={{
          uri: `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${
            details.name
          }.png`,
        }}
      />
      <Text style={styles.pokemonName}>{details.id} - {details.name.toUpperCase()}</Text>
      <View>
        <Text style={styles.description}>
          {details.name} is a pokemon  with {details.weight/10}Kgs. Height: {details.height/10}m, Base Experience: {details.base_experience}
        </Text>
      </View>
      <View >
        <View ><Text style={styles.textLabel}>STATS</Text></View>
      </View>
      <View style={{flex: 1, flexDirection: 'column', marginLeft:10, alignContent: 'flex-start', alignItems: 'flex-start'}}>
        {details.stats.map(stat =>
          <View style={styles.container2} key={stat.stat.name}>
            <Text style={styles.pokemonTypeLabel}><Text style={styles.pokemonType}>{stat.stat.name.toUpperCase()}:</Text> {stat.base_stat}</Text>
          </View>
        )}
      </View>

        <View >
          <View ><Text style={styles.textLabel}>ABILITY</Text></View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginLeft:10}}>
          {details.abilities.map(ability =>
            <View style={styles.container2} key={ability.ability.name}>
              <Image source={PokemonTypeImage['default']} />
              <Text>{ability.ability.name.toUpperCase()}</Text>
            </View>
          )}
        </View>
        <View >
          <View ><Text style={styles.textLabel}>TYPES</Text></View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginLeft:10}}>
          {details.types.map(type =>
            <View style={styles.container2} key={type.type.name}>
              <Image source={PokemonTypeImage[type.type.name.toLowerCase()] || PokemonTypeImage['default']} />
              <Text>{type.type.name.toUpperCase()}</Text>
            </View>
          )}
        </View>
    </View>
  ) : (
    <View style={styles.indicator}>
      <ActivityIndicator size="large" color="#E63F34" />
    </View>
  );
};

export default Details;

const styles = StyleSheet.create({
  container1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#000'
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  text: {
    fontSize: 17,
  },
  textLabel: {
    fontSize: 22,
    marginTop: 10,
  },
  indicator: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  pokemonName: {
    // position: 'absolute',
    marginTop: -10,
    alignSelf: 'center',
    fontSize: 30,
    fontFamily: 'Avenir',
    color: '#4f4f4f',
  },
  avatar: {
    width: 200,
    height: 200,
    alignSelf: 'center',
    top: 5,
  },
  description: {
    color: '#4F4F4F',
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 15,
    marginBottom: 35,
  },
  pokemonTypeLabel: {
    alignItems: 'flex-start',
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: "bold"
  },
  pokemonType: {
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  }
});