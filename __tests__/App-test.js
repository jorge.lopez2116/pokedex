/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import Pokemon from '../src/Components/Pokemons'
import Details from '../src/Components/Details'

it('renders correctly Pokemon View', () => {
  try {
    const pokemon = renderer.create(<Pokemon />).toJSON();
    expect(pokemon).toMatchSnapshot().toThrow("Bad developer!").andCatch(" ERROR ");
  } catch (e) {
    console.log(`\x1b[31mWARNING!!! Catch snapshot failure here and print some message about it...`);
    throw e;
  }
});

it('renders correctly Details Pokemon View', () => {
  try {
    const detail = renderer.create(<Details pokemon={"charmander"} />).toJSON();
    expect(detail).toMatchSnapshot().toThrow("Bad developer!").andCatch(" ERROR ");
  } catch (e) {
    console.log(`\x1b[31mWARNING!!! Catch snapshot failure here and print some message about it...`);
    throw e;
  }
});
