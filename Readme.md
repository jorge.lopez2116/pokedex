# Getting Started Pokedex React Native App by Jorge Lopez

This project is a little test using the API http://pokeapi.co.

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all the dependencies necesaries to run the project

### `react-native run-android`

Runs the app in the Android Device.\

### `react-native run-ios`

Runs the app in the iOS Device.\

## Testing Best Practices

#### Unit testing:
This is the basic kind of testing followed for mobile apps. This includes testing JavaScript objects and methods which are present at the component level.

#### Component testing:
It is possible to test each of the components functionally or visually. In order to test React components, ReactTestUtils offers a simple framework.

#### Integration testing:
The next comes is integration testing. In this phase, a bunch of different units are tested together as one component or entity.

#### Functional testing:
This is a kind of black-box testing which is focused on user interactions and requirements. It covers all the user interactions, underlying software the application as a single entity.

### Tools for Testing:
#### Jest

## Running Jest Test
#### Jest -u

* We use a JEST (Unit Testing) to runing 2 test to comprobate if the views runs correctly
* See the result on: "_test/__snapshots__" 
* Another test to implement is the functional test, because it´s a small App with a little functions
* We use Continius Integration (CI) App Center or Bitrise